package sh4j.ui;

import sh4j.model.browser.SStatus;

import java.awt.BorderLayout;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

// TODO: Auto-generated Javadoc
/**
 * The Class SStatusBar.
 */
public class SStatusBar {
  
  /** The panel. */
  private JPanel panel;
  
  /** The text field. */
  private JTextField textField;
  
  /** The ss. */
  private JList<? extends SStatus> ss;

  /**
   * Instantiates a new s status bar.
   *
   * @param statuses the statuses
   */
  public SStatusBar(SStatus... statuses) {
    panel = new JPanel();
    ss = new JList<>(statuses);
    panel.setLayout(new BorderLayout());
    add(ss, BorderLayout.SOUTH);

  }

  /**
   * Render.
   */
  public void render() {

  }

  /**
   * Adds the.
   *
   * @param list the list
   * @param direction the direction
   */
  public void add(JList<? extends SStatus> list, String direction) {
    list.setLayoutOrientation(JList.VERTICAL);
    list.setCellRenderer(new SItemRenderer());
    JScrollPane pane = new JScrollPane(list);
    pane.setMinimumSize(new java.awt.Dimension(600, 50));
    pane.setMaximumSize(new java.awt.Dimension(600, 100));
    pane.setPreferredSize(new java.awt.Dimension(600, 50));

  }
}
