package sh4j.ui;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SFactory;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SObject;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.model.browser.SStatus;
import sh4j.model.command.SCommand;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SBredStyle;
import sh4j.model.style.SDarkStyle;
import sh4j.model.style.SEclipseStyle;
import sh4j.model.style.SStyle;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

// TODO: Auto-generated Javadoc
/**
 * The Class SFrame.
 */
@SuppressWarnings({"unchecked", "serial"})
public class SFrame extends JFrame implements Observer {

  /** The lighters. */
  private SHighlighter[] lighters;
  
  /** The styles. */
  protected JList<SStyle> styles =
      new JList<SStyle>(new SStyle[] {new SEclipseStyle(), new SDarkStyle(), new SBredStyle()});
  
  /** The style. */
  protected SStyle style;
  
  /** The packages. */
  private JList<SPackage> packages;
  
  /** The classes. */
  private JList<SClass> classes;
  
  /** The methods. */
  private JList<SMethod> methods;
  
  /** The status list. */
  private JList<? extends SStatus> statusList;
  
  /** The html panel. */
  private JEditorPane htmlPanel;
  
  /** The sort. */
  private JMenu sort;
  
  /** The high. */
  private JMenu high;
  
  /** The line number. */
  private JMenu lineNumber;
  
  /** The project. */
  private SProject project;
  
  /** The numbers. */
  private boolean numbers;

  /**
   * Instantiates a new s frame.
   *
   * @param lighters the lighters
   */
  public SFrame(SHighlighter... lighters) {
    super("CC3002 Browser");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setResizable(false);
    setSize(450, 750);
    build();
    style = new SEclipseStyle();
    this.lighters = lighters;
    statusList = new JList<>();
    JMenuBar bar = new JMenuBar();
    setSort(new JMenu("Sort"));
    setHigh(new JMenu("Highlight"));
    setLineNumber(new JMenu("LineNumber"));
    bar.add(getSort());
    bar.add(getHigh());
    bar.add(getLineNumber());
    setJMenuBar(bar);
  }

  /**
   * Adds the commands.
   *
   * @param menu the menu
   * @param cmds the cmds
   */
  public void addCommands(JMenu menu, SCommand... cmds) {
    for (SCommand c : cmds) {
      addCommand(menu, c);
    }
  }

  /**
   * Sets the status.
   *
   * @param statuses the new status
   */
  public void setStatus(SStatus... statuses) {
    statusList = new JList<>(statuses);
  }

  /**
   * Adds the command.
   *
   * @param menu the menu
   * @param c the c
   */
  private void addCommand(JMenu menu, final SCommand c) {
    JMenuItem item = new JMenuItem(c.name());
    item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (getProject() != null) {
          c.executeOn(SFrame.this);
        }
      }
    });
    menu.add(item);
  }

  /**
   * Update.
   *
   * @param list the list
   * @param data the data
   */
  public void update(@SuppressWarnings("rawtypes") JList list,
      @SuppressWarnings("rawtypes") List data) {
    list.setListData(data.toArray(new SObject[] {}));
    if (data.size() > 0) {
      list.setSelectedIndex(0);
    }
  }


  /**
   * Builds the.
   */
  private void build() {
    buildPathPanel();
    methods = new JList<SMethod>();
    addOn(methods, BorderLayout.EAST);
    classes = new JList<SClass>();
    addOn(classes, BorderLayout.CENTER);
    packages = new JList<SPackage>();
    addOn(packages, BorderLayout.WEST);
    htmlPanel = buildHTMLPanel(BorderLayout.SOUTH);
    packages.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (packages.getSelectedIndex() != -1) {
          update(classes, packages.getSelectedValue().classes());
        }
      }
    });
    classes.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (classes.getSelectedIndex() != -1) {
          update(methods, classes.getSelectedValue().methods());
        }
      }
    });
    methods.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (methods.getSelectedIndex() != -1) {
          SMethod method = methods.getSelectedValue();
          htmlPanel.setText(method.body().toHTML(style, numbers, lighters));
        }
      }
    });

  }

  /**
   * Adds the on.
   *
   * @param list the list
   * @param direction the direction
   */
  public void addOn(JList<? extends SObject> list, String direction) {
    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    list.setLayoutOrientation(JList.VERTICAL);
    list.setCellRenderer(new SItemRenderer());
    JScrollPane pane = new JScrollPane(list);
    pane.setMinimumSize(new Dimension(200, 200));
    pane.setPreferredSize(new Dimension(200, 200));
    pane.setMaximumSize(new Dimension(200, 200));
    getContentPane().add(pane, direction);
  }

  /**
   * Builds the path panel.
   */
  private void buildPathPanel() {
    JPanel pathPanel = new JPanel();
    pathPanel.setLayout(new BorderLayout());
    final JTextField pathField = new JTextField();
    pathField.setEnabled(false);
    JButton browseButton = new JButton("Browse");
    browseButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select a root folder");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(SFrame.this) == JFileChooser.APPROVE_OPTION) {
          pathField.setText(chooser.getSelectedFile().getPath());
          try {
            setProject(new SFactory().create(chooser.getSelectedFile().getPath()));
            getProject().addObserver(SFrame.this);
            update(packages, getProject().packages());
            if (getProject().packages().size() > 0) {
              packages.setSelectedIndex(0);
            }
          } catch (IOException ex) {

          }
        }
      }
    });
    pathPanel.add(pathField, BorderLayout.CENTER);
    pathPanel.add(browseButton, BorderLayout.EAST);
    getContentPane().add(pathPanel, BorderLayout.NORTH);
  }


  /**
   * Builds the html panel.
   *
   * @param direction the direction
   * @return the j editor pane
   */
  public JEditorPane buildHTMLPanel(String direction) {
    JEditorPane htmlPanel = new JEditorPane();
    htmlPanel.setEditable(false);
    htmlPanel.setMinimumSize(new Dimension(600, 300));
    htmlPanel.setPreferredSize(new Dimension(600, 300));
    htmlPanel.setMaximumSize(new Dimension(600, 300));
    htmlPanel.setContentType("text/html");

    JEditorPane statusBar = new JEditorPane();
    getContentPane().add(new JScrollPane(htmlPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
        JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), direction);
    htmlPanel.add(new JEditorPane(), BorderLayout.SOUTH);
    return htmlPanel;
  }


  /**
   * Update.
   *
   * @param project the project
   */
  public void update(SProject project) {
    SPackage pkg = packages.getSelectedValue();
    SClass cls = classes.getSelectedValue();
    SMethod method = methods.getSelectedValue();
    packages.setListData(project.packages().toArray(new SPackage[] {}));
    packages.setSelectedValue(pkg, true);
    if (pkg != null) {
      classes.setListData(pkg.classes().toArray(new SClass[] {}));
      classes.setSelectedValue(cls, true);
      if (cls != null) {
        methods.setListData(cls.methods().toArray(new SMethod[] {}));
        methods.setSelectedValue(method, true);
      }
    }
  }

  /**
   * Style.
   *
   * @param style the style
   */
  public void style(SStyle style) {
    this.style = style;
    SMethod method = methods.getSelectedValue();
    htmlPanel.setText(method.body().toHTML(style, numbers, lighters));
  }


  /* (non-Javadoc)
   * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
   */
  @Override
  public void update(Observable o, Object arg) {
    if (o instanceof SProject) {
      SProject project = (SProject) o;
      update(project);
    }
  }

  /**
   * Gets the project.
   *
   * @return the project
   */
  public SProject getProject() {
    return project;
  }

  /**
   * Sets the project.
   *
   * @param project the new project
   */
  public void setProject(SProject project) {
    this.project = project;
  }

  /**
   * Gets the sort.
   *
   * @return the sort
   */
  public JMenu getSort() {
    return sort;
  }

  /**
   * Sets the sort.
   *
   * @param sort the new sort
   */
  public void setSort(JMenu sort) {
    this.sort = sort;
  }

  /**
   * Gets the high.
   *
   * @return the high
   */
  public JMenu getHigh() {
    return high;
  }

  /**
   * Sets the high.
   *
   * @param high the new high
   */
  public void setHigh(JMenu high) {
    this.high = high;
  }

  /**
   * Toggle numbers.
   */
  public void toggleNumbers() {
    numbers = !numbers;
    style(style);

  }

  /**
   * Gets the line number.
   *
   * @return the line number
   */
  public JMenu getLineNumber() {
    return lineNumber;
  }

  /**
   * Sets the line number.
   *
   * @param lineNumber the new line number
   */
  public void setLineNumber(JMenu lineNumber) {
    this.lineNumber = lineNumber;
  }
}
