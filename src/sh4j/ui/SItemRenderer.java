package sh4j.ui;

import sh4j.model.browser.SObject;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

// TODO: Auto-generated Javadoc
/**
 * The Class SItemRenderer.
 */
@SuppressWarnings("serial")
public class SItemRenderer extends JLabel implements ListCellRenderer<SObject> {
  
  /** The default renderer. */
  protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

  /**
   * Instantiates a new s item renderer.
   */
  public SItemRenderer() {
    setOpaque(true);
    setBorder(BorderFactory.createLineBorder(Color.WHITE));
  }

  /* (non-Javadoc)
   * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
   */
  @Override
  public Component getListCellRendererComponent(JList<? extends SObject> list, SObject value,
      int index, boolean isSelected, boolean cellHasFocus) {
    setText(value.toString());
    setFont(value.font());
    setIcon(new ImageIcon(value.icon()));
    setBackground(value.background());


    if (isSelected) {
      setBorder(BorderFactory.createLineBorder(Color.BLUE));
    } else {
      setBorder(BorderFactory.createLineBorder(Color.WHITE));
    }
    return this;
  }
}
