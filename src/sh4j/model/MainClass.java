package sh4j.model;

import sh4j.model.browser.linesOfCode;
import sh4j.model.command.SBredHigh;
import sh4j.model.command.SDarkHigh;
import sh4j.model.command.SEclipseHigh;
import sh4j.model.command.SSetLineNumbers;
import sh4j.model.command.SSortClassesByHierarchy;
import sh4j.model.command.SSortClassesByName;
import sh4j.model.command.SSortPackagesByName;
import sh4j.model.highlight.SClassName;
import sh4j.model.highlight.SCurlyBracket;
import sh4j.model.highlight.SKeyWord;
import sh4j.model.highlight.SMainClass;
import sh4j.model.highlight.SModifier;
import sh4j.model.highlight.SPseudoVariable;
import sh4j.model.highlight.SSemiColon;
import sh4j.model.highlight.SString;
import sh4j.ui.SFrame;

import java.io.File;
import java.io.IOException;

// TODO: Auto-generated Javadoc
/**
 * The Class MainClass.
 *
 * @author pablo
 */
public abstract class MainClass {
  
  /**
   * The main method.
   *
   * @param args the arguments
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public static void main(String[] args) throws IOException {
    SFrame frame = new SFrame(new SClassName(), new SCurlyBracket(), new SKeyWord(),
        new SMainClass(), new SModifier(), new SPseudoVariable(), new SSemiColon(), new SString());
    frame.pack();
    frame.setVisible(true);
    frame.addCommands(frame.getSort(), new SSortClassesByHierarchy(), new SSortClassesByName(),
        new SSortPackagesByName());
    frame.addCommands(frame.getHigh(), new SEclipseHigh(), new SDarkHigh(), new SBredHigh());
    frame.addCommands(frame.getLineNumber(), new SSetLineNumbers());
    frame.setStatus(new linesOfCode());
    System.out.println(File.separator);

  }
}
