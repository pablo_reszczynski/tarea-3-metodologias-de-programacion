package sh4j.model.style;

// TODO: Auto-generated Javadoc
/**
 * The Class SBredStyle.
 */
public class SBredStyle implements SStyle {

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "bred";
  }

  /* (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatClassName(java.lang.String)
   */
  @Override
  public String formatClassName(String text) {
    return text;
  }

  /* (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatCurlyBracket(java.lang.String)
   */
  @Override
  public String formatCurlyBracket(String text) {
    return "<span style='color:#806030; '>" + text + "</span>";
  }

  /* (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatKeyWord(java.lang.String)
   */
  @Override
  public String formatKeyWord(String text) {
    return "<span style='color:#800040; '>" + text + "</span>";
  }

  /* (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatPseudoVariable(java.lang.String)
   */
  @Override
  public String formatPseudoVariable(String text) {
    return "<span style='color:#400000; font-weight:bold; '>" + text + "</span>";
  }

  /* (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatSemiColon(java.lang.String)
   */
  @Override
  public String formatSemiColon(String text) {
    return "<span style='color:#806030; '>" + text + "</span>";
  }

  /* (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatString(java.lang.String)
   */
  @Override
  public String formatString(String text) {
    return "<span style='color:#e60000; '>" + text + "</span>";
  }

  /* (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatMainClass(java.lang.String)
   */
  @Override
  public String formatMainClass(String text) {
    return "<span style='color:#800040; '>" + text + "</span>";
  }

  /* (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatBody(java.lang.String)
   */
  @Override
  public String formatBody(String text) {
    return "<pre style='color:#000000;background:#f1f0f0;'>" + text + "</pre>";
  }

  /* (non-Javadoc)
   * @see sh4j.model.style.SStyle#formatModifier(java.lang.String)
   */
  @Override
  public String formatModifier(String text) {
    return "<span style='color:#400000; font-weight:bold; '>" + text + "</span>";
  }

}
