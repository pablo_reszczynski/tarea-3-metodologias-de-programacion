package sh4j.model.style;

// TODO: Auto-generated Javadoc
/**
 * The Interface SStyle.
 */
public interface SStyle {

  /**
   * To string.
   *
   * @return the string
   */
  public String toString();

  /**
   * Format class name.
   *
   * @param text the text
   * @return the string
   */
  public String formatClassName(String text);

  /**
   * Format curly bracket.
   *
   * @param text the text
   * @return the string
   */
  public String formatCurlyBracket(String text);

  /**
   * Format key word.
   *
   * @param text the text
   * @return the string
   */
  public String formatKeyWord(String text);

  /**
   * Format pseudo variable.
   *
   * @param text the text
   * @return the string
   */
  public String formatPseudoVariable(String text);

  /**
   * Format semi colon.
   *
   * @param text the text
   * @return the string
   */
  public String formatSemiColon(String text);

  /**
   * Format string.
   *
   * @param text the text
   * @return the string
   */
  public String formatString(String text);

  /**
   * Format main class.
   *
   * @param text the text
   * @return the string
   */
  public String formatMainClass(String text);

  /**
   * Format modifier.
   *
   * @param text the text
   * @return the string
   */
  public String formatModifier(String text);

  /**
   * Format body.
   *
   * @param text the text
   * @return the string
   */
  public String formatBody(String text);


}
