package sh4j.model.command;

import sh4j.model.style.SBredStyle;
import sh4j.model.style.SStyle;
import sh4j.ui.SFrame;

// TODO: Auto-generated Javadoc
/**
 * The Class SBredHigh.
 */
public class SBredHigh extends SCommand {
  
  /** The style. */
  final SStyle style = new SBredStyle();

  /* (non-Javadoc)
   * @see sh4j.model.command.SCommand#executeOn(sh4j.ui.SFrame)
   */
  @Override
  public void executeOn(SFrame frame) {
    frame.style(style);
  }

}
