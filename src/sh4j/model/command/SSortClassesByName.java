package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.ui.SFrame;

import java.util.Collections;
import java.util.Comparator;

// TODO: Auto-generated Javadoc
/**
 * The Class SSortClassesByName.
 */
public class SSortClassesByName extends SCommand {


  /* (non-Javadoc)
   * @see sh4j.model.command.SCommand#executeOn(sh4j.ui.SFrame)
   */
  @Override
  public void executeOn(SFrame frame) {

    for (SPackage pkg : frame.getProject().packages()) {
      Collections.sort(pkg.classes(), new Comparator<SClass>() {
        @Override
        public int compare(SClass o1, SClass o2) {
          return o1.toString().compareTo(o2.toString());
        }
      });
    }
    frame.getProject().changed();
  }
}
