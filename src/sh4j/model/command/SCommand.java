package sh4j.model.command;


import sh4j.ui.SFrame;

// TODO: Auto-generated Javadoc
/**
 * The Class SCommand.
 */
public abstract class SCommand {
  
  /**
   * Execute on.
   *
   * @param frame the frame
   */
  public abstract void executeOn(SFrame frame);

  /**
   * Name.
   *
   * @return the string
   */
  public String name() {
    return this.getClass().getName();
  }
}
