package sh4j.model.command;

import sh4j.model.browser.SPackage;
import sh4j.ui.SFrame;

import java.util.Collections;
import java.util.Comparator;

// TODO: Auto-generated Javadoc
/**
 * The Class SSortPackagesByName.
 */
public class SSortPackagesByName extends SCommand {


  /* (non-Javadoc)
   * @see sh4j.model.command.SCommand#executeOn(sh4j.ui.SFrame)
   */
  @Override
  public void executeOn(SFrame frame) {
    Collections.sort(frame.getProject().packages(), new Comparator<SPackage>() {
      @Override
      public int compare(SPackage o1, SPackage o2) {
        return o1.toString().compareTo(o2.toString());
      }
    });
    frame.getProject().changed();
  }

}
