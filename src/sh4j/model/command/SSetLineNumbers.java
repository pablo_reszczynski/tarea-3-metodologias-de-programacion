package sh4j.model.command;

import sh4j.ui.SFrame;

// TODO: Auto-generated Javadoc
/**
 * The Class SSetLineNumbers.
 */
public class SSetLineNumbers extends SCommand {

  /* (non-Javadoc)
   * @see sh4j.model.command.SCommand#executeOn(sh4j.ui.SFrame)
   */
  @Override
  public void executeOn(SFrame frame) {
    frame.toggleNumbers();
  }

}
