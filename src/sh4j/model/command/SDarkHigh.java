package sh4j.model.command;

import sh4j.model.style.SDarkStyle;
import sh4j.model.style.SStyle;
import sh4j.ui.SFrame;

// TODO: Auto-generated Javadoc
/**
 * The Class SDarkHigh.
 */
public class SDarkHigh extends SCommand {
  
  /** The style. */
  final SStyle style = new SDarkStyle();

  /* (non-Javadoc)
   * @see sh4j.model.command.SCommand#executeOn(sh4j.ui.SFrame)
   */
  @Override
  public void executeOn(SFrame frame) {
    frame.style(style);
  }

}
