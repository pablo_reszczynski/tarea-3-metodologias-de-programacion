package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.ui.SFrame;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class SSortClassesByHierarchy.
 */
public class SSortClassesByHierarchy extends SCommand {

  /* (non-Javadoc)
   * @see sh4j.model.command.SCommand#executeOn(sh4j.ui.SFrame)
   */
  @Override
  public void executeOn(SFrame frame) {
    (new SSortClassesByName()).executeOn(frame);
    for (final SPackage pkg : frame.getProject().packages()) {
      Collections.sort(pkg.classes(), new Comparator<SClass>() {
        @Override
        public int compare(SClass o1, SClass o2) {
          String path1 = path(pkg.classes(), o1);
          String path2 = path(pkg.classes(), o2);
          return path1.compareTo(path2);
        }
      });
    }
    frame.getProject().changed();
  }

  /**
   * Path.
   *
   * @param cls the cls
   * @param theClass the the class
   * @return the string
   */
  public String path(List<SClass> cls, SClass theClass) {
    SClass parent = get(cls, theClass.superClass());
    if (parent == null) {
      return theClass.toString();
    } else {
      return path(cls, parent) + theClass.toString();
    }
  }

  /**
   * Gets the.
   *
   * @param cls the cls
   * @param theClass the the class
   * @return the s class
   */
  public SClass get(List<SClass> cls, String theClass) {
    for (SClass c : cls) {
      if (c.toString().equals(theClass)) {
        return c;
      }
    }
    return null;
  }

}
