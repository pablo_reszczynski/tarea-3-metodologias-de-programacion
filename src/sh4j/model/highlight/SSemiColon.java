package sh4j.model.highlight;

import sh4j.model.style.SStyle;



// TODO: Auto-generated Javadoc
/**
 * The Class SSemiColon.
 */
public class SSemiColon implements SHighlighter {

  /* (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#needsHighLight(java.lang.String)
   */
  @Override
  public boolean needsHighLight(String text) {
    return ";".equals(text);
  }

  /* (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#highlight(java.lang.String, sh4j.model.style.SStyle)
   */
  @Override
  public String highlight(String text, SStyle style) {
    return style.formatSemiColon(text);
  }

}
