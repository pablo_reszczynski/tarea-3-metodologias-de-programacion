package sh4j.model.highlight;

import sh4j.model.style.SStyle;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * The Class SMainClass.
 */
public class SMainClass implements SHighlighter {
  
  /** The Constant javaMainClasses. */
  private static final Set<String> javaMainClasses =
      new HashSet<String>(Arrays.asList("System", "Character", "String"));

  /* (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#needsHighLight(java.lang.String)
   */
  @Override
  public boolean needsHighLight(String text) {
    return javaMainClasses.contains(text);
  }

  /* (non-Javadoc)
   * @see sh4j.model.highlight.SHighlighter#highlight(java.lang.String, sh4j.model.style.SStyle)
   */
  @Override
  public String highlight(String text, SStyle style) {
    return style.formatMainClass(text);
  }
}
