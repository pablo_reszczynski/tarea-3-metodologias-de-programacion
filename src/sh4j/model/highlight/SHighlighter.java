package sh4j.model.highlight;

import sh4j.model.style.SStyle;

// TODO: Auto-generated Javadoc
/**
 * The Interface SHighlighter.
 */
public interface SHighlighter {
  
  /**
   * Needs high light.
   *
   * @param text the text
   * @return true, if successful
   */
  public boolean needsHighLight(String text);

  /**
   * Highlight.
   *
   * @param text the text
   * @param style the style
   * @return the string
   */
  public String highlight(String text, SStyle style);
}
