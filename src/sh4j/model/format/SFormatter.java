package sh4j.model.format;

import sh4j.parser.model.SBlock;

// TODO: Auto-generated Javadoc
/**
 * The Interface SFormatter.
 */
public interface SFormatter {
  
  /**
   * Styled word.
   *
   * @param word the word
   */
  public void styledWord(String word);

  /**
   * Styled char.
   *
   * @param character the character
   */
  public void styledChar(char character);

  /**
   * Styled space.
   */
  public void styledSpace();

  /**
   * Styled cr.
   */
  public void styledCR();

  /**
   * Styled block.
   *
   * @param block the block
   */
  public void styledBlock(SBlock block);

  /**
   * Formatted text.
   *
   * @return the string
   */
  public String formattedText();
}
