package sh4j.model.format;

import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;

// TODO: Auto-generated Javadoc
/**
 * The Class SPlainFormatter.
 */
public class SPlainFormatter implements SFormatter {

  /** The buffer. */
  final StringBuffer buffer;
  
  /** The level. */
  private int level;

  /**
   * Instantiates a new s plain formatter.
   */
  public SPlainFormatter() {
    buffer = new StringBuffer();
  }

  /* (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledWord(java.lang.String)
   */
  @Override
  public void styledWord(String word) {
    buffer.append(word);
  }

  /* (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledChar(char)
   */
  @Override
  public void styledChar(char character) {
    buffer.append(character);
  }

  /* (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledSpace()
   */
  @Override
  public void styledSpace() {
    buffer.append(' ');
  }

  /* (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledCR()
   */
  @Override
  public void styledCR() {
    buffer.append('\n');
    indent();
  }

  /* (non-Javadoc)
   * @see sh4j.model.format.SFormatter#styledBlock(sh4j.parser.model.SBlock)
   */
  @Override
  public void styledBlock(SBlock block) {
    level++;
    for (SText text : block.texts()) {
      text.export(this);
    }
    level--;
  }

  /**
   * Indent.
   */
  public void indent() {
    for (int i = 0; i < level; i++) {
      buffer.append("  ");
    }
  }

  /* (non-Javadoc)
   * @see sh4j.model.format.SFormatter#formattedText()
   */
  @Override
  public String formattedText() {
    return buffer.toString();
  }

}
