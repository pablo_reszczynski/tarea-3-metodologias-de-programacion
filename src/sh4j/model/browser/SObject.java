package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;
import java.util.Observable;

// TODO: Auto-generated Javadoc
/**
 * The Class SObject.
 */
public class SObject extends Observable {

  /**
   * Font.
   *
   * @return the font
   */
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

  /**
   * Icon.
   *
   * @return the string
   */
  public String icon() {
    return "";
  }

  /**
   * Background.
   *
   * @return the color
   */
  public Color background() {
    return Color.WHITE;
  }

  /**
   * Changed.
   */
  public void changed() {
    setChanged();
    notifyObservers();
  }

}
