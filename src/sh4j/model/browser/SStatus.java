package sh4j.model.browser;

// TODO: Auto-generated Javadoc
/**
 * The Class SStatus.
 */
public abstract class SStatus extends SObject {
  
  /**
   * Gets the name.
   *
   * @return the name
   */
  abstract String getName();
}
