package sh4j.model.browser;

import sh4j.parser.SClassParser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating S objects.
 */
public class SFactory {

  /**
   * Creates the.
   *
   * @param path the path
   * @return the s project
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public SProject create(String path) throws IOException {
    File directory = new File(path);
    List<File> folders = new ArrayList<File>();
    fillFolders(folders, directory);
    SProject project = new SProject();
    for (File file : folders) {
      project.addPackage(createPackage(file, directory.getCanonicalPath()));
    }
    return project;
  }

  /**
   * Creates a new S object.
   *
   * @param root the root
   * @param path the path
   * @return the s package
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public SPackage createPackage(File root, String path) throws IOException {
    SPackage pack = new SPackage(root.getCanonicalPath().replace(path, ""));
    for (File file : root.listFiles()) {
      if (file.isFile() && file.getName().endsWith(".java")) {
        List<SClass> classes = createClass(file);
        for (SClass cls : classes) {
          pack.addClass(cls);
        }
      }
    }
    return pack;
  }


  /**
   * Creates a new S object.
   *
   * @param root the root
   * @return the list< s class>
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public List<SClass> createClass(File root) throws IOException {
    Path path = Paths.get(root.getPath());
    Charset charset = Charset.forName("ISO-8859-1");
    List<String> lines = Files.readAllLines(path, charset);
    StringBuilder code = new StringBuilder();
    for (String line : lines) {
      code.append(line);
    }
    return SClassParser.parse(code.toString());
  }

  /**
   * Fill folders.
   *
   * @param folders the folders
   * @param root the root
   */
  private void fillFolders(List<File> folders, File root) {
    if (root.isDirectory()) {
      folders.add(root);
      for (File file : root.listFiles()) {
        fillFolders(folders, file);
      }
    }
  }
}
