package sh4j.parser;

import sh4j.model.browser.SClass;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.TypeDeclaration;

// TODO: Auto-generated Javadoc
/**
 * The Class SClassParser.
 */
public class SClassParser extends ASTVisitor {
  
  /** The classes. */
  private List<SClass> classes;

  /**
   * Instantiates a new s class parser.
   */
  public SClassParser() {
    classes = new ArrayList<SClass>();
  }

  /* (non-Javadoc)
   * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.TypeDeclaration)
   */
  public boolean visit(TypeDeclaration node) {
    SMethodParser parser = new SMethodParser();
    node.accept(parser);
    classes.add(new SClass(node, parser.methods()));
    return super.visit(node);
  }

  /**
   * Classes.
   *
   * @return the list
   */
  public List<SClass> classes() {
    return classes;
  }

  /**
   * Parses the.
   *
   * @param source the source
   * @return the list
   */
  public static List<SClass> parse(String source) {
    ASTParser parser = ASTParser.newParser(AST.JLS8);
    parser.setSource(source.toCharArray());
    parser.setKind(ASTParser.K_COMPILATION_UNIT);
    final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
    SClassParser builder = new SClassParser();
    cu.accept(builder);
    return builder.classes();
  }

}
