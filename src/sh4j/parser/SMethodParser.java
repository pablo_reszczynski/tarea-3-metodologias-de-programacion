package sh4j.parser;

import sh4j.model.browser.SMethod;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodDeclaration;

// TODO: Auto-generated Javadoc
/**
 * The Class SMethodParser.
 */
public class SMethodParser extends ASTVisitor {
  
  /** The methods. */
  private List<SMethod> methods;

  /**
   * Instantiates a new s method parser.
   */
  public SMethodParser() {
    methods = new ArrayList<SMethod>();
  }

  /* (non-Javadoc)
   * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.MethodDeclaration)
   */
  public boolean visit(MethodDeclaration node) {
    SASTParser parser = new SASTParser();
    node.accept(parser);
    methods.add(new SMethod(node, parser.top()));
    return super.visit(node);
  }

  /**
   * Methods.
   *
   * @return the list
   */
  public List<SMethod> methods() {
    return methods;
  }
}
