package sh4j.parser.model;

import sh4j.model.format.SFormatter;

// TODO: Auto-generated Javadoc
/**
 * The Class SSpace.
 */
public class SSpace extends SText {

  /* (non-Javadoc)
   * @see sh4j.parser.model.SText#export(sh4j.model.format.SFormatter)
   */
  @Override
  public void export(SFormatter format) {
    format.styledSpace();
  }

}
