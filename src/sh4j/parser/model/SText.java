package sh4j.parser.model;


import sh4j.model.format.SFormatter;
import sh4j.model.format.SHTMLFormatter;
import sh4j.model.format.SPlainFormatter;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;

// TODO: Auto-generated Javadoc
/**
 * The Class SText.
 */
public abstract class SText {

  /**
   * Export.
   *
   * @param format the format
   */
  public abstract void export(SFormatter format);

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString() {
    SFormatter format = new SPlainFormatter();
    this.export(format);
    return format.formattedText();
  }

  /**
   * To html.
   *
   * @param style the style
   * @param numbers the numbers
   * @param lighters the lighters
   * @return the string
   */
  public String toHTML(SStyle style, boolean numbers, SHighlighter... lighters) {
    SFormatter format = new SHTMLFormatter(style, numbers, lighters);
    this.export(format);
    return format.formattedText();
  }
}
